// HSKEY.h

#if !defined(__KEYCONFIG_H__)
#define __KEYCONFIG_H__

typedef struct _KEYMAP
{
	char psKeyName[32];
	unsigned int nKeyValue;
}KEYMAP;

#define KEY_MAX 64

class CKeyConfig  
{
private:
	int m_nCount;
	struct _KEYMAP m_pstKeyMap[KEY_MAX];

public:
	CKeyConfig();
	virtual ~CKeyConfig();

public:
	BOOL Load(char* psFileName);
	int  Find(char* psKeyName);
	int  GetCount();
	BOOL Get(int nIndex, char* psKeyName);
};

#endif // !defined(__KEYCONFIG_H__)
