// OApp.h: interface for the OApp class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OAPP_H__)
#define __OAPP_H__


class OApp
{
private:
	BOOL m_bReverseShow; // 是否翻转显示

public:
	LCD*           m_pLCD;             // 显示设备对象
	LCD*           m_pScreenBuffer;    // 显示缓冲区
	OMsgQueue*     m_pMsgQ;            // 消息队列
	OWindow*       m_pMainWnd;         // 主窗口
	OCaret*        m_pCaret;           // 脱字符
	OTimerQueue*   m_pTimerQ;          // 定时器队列
	OWindow*       m_pTopMostWnd;      // TopMost窗口
	OClock*        m_pClock;           // 钟表窗口
	OSystemBar*    m_pSysBar;          // 系统状态条
	KeyMap*        m_pKeyMap;          // 按键宏与按键值对照表
	BWImgMgt*      m_pImageMgt;        // 图像资源管理类

#if defined (CHINESE_SUPPORT)
	OIME*          m_pIMEWnd;          // 输入法窗口
#endif // defined(CHINESE_SUPPORT)

	BOOL m_bClockKeyEnable;            // 是否使能时钟窗口

public:
	OApp();
	virtual ~OApp();

	BOOL ReverseShow(void);

	// 对指定的窗口进行绘制
	virtual void PaintWindows (OWindow* pWnd);

#if defined (RUN_ENVIRONMENT_WIN32)
	HWND m_hWnd;
	virtual BOOL Create (OWindow* pMainWnd, HWND hWnd);
#endif // defined(RUN_ENVIRONMENT_WIN32)

	virtual BOOL Run();

#if defined (RUN_ENVIRONMENT_WIN32)
	// 专用于Win32仿真环境
	void RunInWin32 ();
#endif // defined(RUN_ENVIRONMENT_WIN32)

	// 消息队列空状态下的处理
	virtual void Idle();

	// 显示开机画面
	virtual void ShowStart ();

	// 空闲处理
	virtual void OnIdle ();

	// 蜂鸣器鸣叫指示检测到按键按键消息
	virtual void Beep ();

	// 刷新显示
	virtual void Show ();

	// 获取键盘事件，插入消息队列；
	// 直接将键盘键码作为参数插入消息队列；
	// 如果该键值是按键按下的值(值小于128)
	// 则调用IAL层的getch函数实现。iMsg = OM_KEYDOWN；WPARAM = 键值
	virtual BOOL GetKeyboardEvent();

	void PostKeyMsg (BYTE nKeyValue);

	// 清除按键缓冲区残留的内容，用于比较耗时的操作结束时
	BOOL CleanKeyBuffer ();

	// 发送消息；
	// 区分出OM_PAINT消息单独处理；
	int DespatchMsg (O_MSG* pMsg);

#if defined (RUN_ENVIRONMENT_WIN32)
	// 模仿GetKeyboardEvent函数向MonoGUI系统的消息队列中插入一个键盘消息
	// 该函数仅用于windows下仿真,在Win32下替换掉DespatchMsg函数
	void InsertMsgToMonoGUI (MSG* pMSG);
#endif // defined(RUN_ENVIRONMENT_WIN32)

// 下面的函数调用成员类的相应函数实现

#if defined (CHINESE_SUPPORT)
	// 打开输入法窗口(打开显示，创建联系)
	BOOL OpenIME (OWindow* pWnd);
#endif // defined(CHINESE_SUPPORT)

#if defined (CHINESE_SUPPORT)
	// 关闭输入法窗口(关闭显示，断开联系)
	BOOL CloseIME (OWindow* pWnd);
#endif // defined(CHINESE_SUPPORT)

	// 显示系统忙信息
	BOOL ShowHint (int nIcon, char* sInfo);

	// 关闭系统提示
	BOOL CloseHint();

	// 直接调用消息处理函数；
	int SendMsg (O_MSG* pMsg);

	// 向消息队列中添加一条消息；
	// 如果消息队列满（消息数量达到了MESSAGE_MAX 所定义的数目），则返回失败；
	BOOL PostMsg (O_MSG* pMsg);

	// 向消息队列中添加一条退出消息；
	BOOL PostQuitMsg();

	// 在消息队列中查找指定类型的消息；
	// 如果发现消息队列中有指定类型的消息，则返回TRUE；
	// 该函数主要用在定时器处理上。CheckTimer函数首先检查消息队列中有没有相同的定时器消息，如果没有，再插入新的定时器消息
	BOOL FindMsg (O_MSG* pMsg);

	// 根据窗口的脱字符信息设置系统脱字符的参数；
	BOOL SetCaret (CARET* pCaret);

	// 添加一个定时器；
	// 如果当前定时器的数量已经达到TIMER_MAX所定义的数目，则返回FALSE；
	// 如果发现一个ID与当前定时器相同的定时器，则直接修改该定时器的设定；
	BOOL SetTimer (OWindow* pWindow, int nTimerID, int interval);

	// 删除一个定时器；
	// 根据TimerID删除
	BOOL KillTimer (int nTimerID);

#if defined (CHINESE_SUPPORT)
	// 看输入法窗口是否处于打开状态
	BOOL IsIMEOpen();
#endif // defined(CHINESE_SUPPORT)

	// 设置TopMost窗口
	BOOL SetTopMost (OWindow* pWindow);

	// 检验一个窗口指针是否有效
	BOOL IsWindowValid (OWindow* pWindow);

	// 使能/禁止显示时钟
	// 注：显示时钟使能时，按“当前时间”键打开时钟窗口。
	// “当前时间”按键消息将不能发送给任何一个窗口。
	// 机器自检时，需要禁止始终，以保证自检窗口可以接收到“当前时间”的按键消息
	BOOL ClockKeyEnable (BOOL bEnable);

	// 设置时钟的显示位置
	BOOL SetClockPos (int x, int y);

#if defined (MOUSE_SUPPORT)
	// 设置时钟按钮的位置
	BOOL SetClockButton (int x, int y);
#endif // defined(MOUSE_SUPPORT)
};

#endif // !defined(__OAPP_H__)
