#include "stdafx.h"
#include "BmpTools.h"

#include <iostream>
#include <fstream>
#include <string>
using namespace std;


// http://blog.sina.com.cn/s/blog_6ee382440100moeu.html
// http://apps.hi.baidu.com/share/detail/32878068
// bitmap 位图CBitmap对象指针
// lpFileName 为位图文件名
bool SaveBitmapToFile(CBitmap* bitmap, LPWSTR lpFileName)
{
    HBITMAP hBitmap;   //为刚才的屏幕位图句柄
    HDC hDC;           //设备描述表
    int iBits;         //当前显示分辨率下每个像素所占字节数
    WORD wBitCount;    //位图中每个像素所占字节数
    DWORD dwPaletteSize = 0, //定义调色板大小
        dwBmBitsSize,  //位图中像素字节大小
        dwDIBSize,     //位图文件大小
        dwWritten;     //写入文件字节数
    BITMAP Bitmap;     //位图属性结构
    BITMAPFILEHEADER bmfHdr; //位图文件头结构
    BITMAPINFOHEADER bi; //位图信息头结构
    LPBITMAPINFOHEADER lpbi; //指向位图信息头结构
    HANDLE fh,         //定义文件
        hDib,          //分配内存句柄
        hPal,          //调色板句柄
        hOldPal = NULL;
    
    //计算位图文件每个像素所占字节数
    hBitmap = (HBITMAP)*bitmap;
    hDC = CreateDC(_T("DISPLAY"), NULL, NULL, NULL);
    iBits = GetDeviceCaps(hDC, BITSPIXEL) * GetDeviceCaps(hDC, PLANES);
    DeleteDC(hDC);
    
    if (iBits <= 1)
        wBitCount = 1;
    else if (iBits <= 4)
        wBitCount = 4;
    else if (iBits <= 8)
        wBitCount = 8;
    else if (iBits <= 24)
        wBitCount = 24;
    else if (iBits <= 32)
        wBitCount = 32;
    
    //计算调色板大小
    if (wBitCount <= 8)
        dwPaletteSize = (1 << wBitCount) * sizeof (RGBQUAD);
    
    //设置位图信息头结构
    GetObject(hBitmap, sizeof (BITMAP), (LPSTR)&Bitmap);
    bi.biSize = sizeof (BITMAPINFOHEADER);
    bi.biWidth = Bitmap.bmWidth;
    bi.biHeight = Bitmap.bmHeight;
    bi.biPlanes = 1;
    bi.biBitCount = wBitCount;
    bi.biCompression = BI_RGB;
    bi.biSizeImage = 0;
    bi.biXPelsPerMeter = 0;
    bi.biYPelsPerMeter = 0;
    bi.biClrUsed = 0;
    bi.biClrImportant = 0;
    
    dwBmBitsSize = ((Bitmap.bmWidth * wBitCount+31) / 32) * 4 * Bitmap.bmHeight;
    
    //为位图内容分配内存
    hDib = GlobalAlloc(GHND, dwBmBitsSize + dwPaletteSize + sizeof (BITMAPINFOHEADER));
    lpbi = (LPBITMAPINFOHEADER)GlobalLock(hDib);
    *lpbi = bi;
    
    // 处理调色板
    hPal = GetStockObject(DEFAULT_PALETTE);
    if (hPal)
    {
        hDC = ::GetDC(NULL);
        hOldPal = ::SelectPalette(hDC, (HPALETTE)hPal, FALSE);
        RealizePalette(hDC);
    }
    
    // 获取该调色板下新的像素值
    GetDIBits(hDC, hBitmap, 0, (UINT) Bitmap.bmHeight,
        (LPSTR)lpbi + sizeof (BITMAPINFOHEADER) + dwPaletteSize,
        (LPBITMAPINFO)lpbi, DIB_RGB_COLORS);
    
    //恢复调色板
    if (hOldPal)
    {
        SelectPalette(hDC, (HPALETTE)hOldPal, TRUE);
        RealizePalette(hDC);
        ::ReleaseDC(NULL, hDC);
    }
    
    //创建位图文件
    fh = CreateFile(lpFileName, GENERIC_WRITE,
        0, NULL, CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
    
    if (fh == INVALID_HANDLE_VALUE)
        return FALSE;
    
    //设置位图文件头
    bmfHdr.bfType = 0x4D42;     //"BM"
    dwDIBSize = sizeof (BITMAPFILEHEADER)
        + sizeof (BITMAPINFOHEADER)
        + dwPaletteSize + dwBmBitsSize;
    bmfHdr.bfSize = dwDIBSize;
    bmfHdr.bfReserved1 = 0;
    bmfHdr.bfReserved2 = 0;
    bmfHdr.bfOffBits = (DWORD)sizeof (BITMAPFILEHEADER)
        + (DWORD)sizeof (BITMAPINFOHEADER)
        + dwPaletteSize;
    
    //写入位图文件头
    WriteFile(fh, (LPSTR)&bmfHdr, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);
    
    //写入位图文件其余内容
    WriteFile(fh, (LPSTR)lpbi, dwDIBSize,
        &dwWritten, NULL);
    
    //清除
    GlobalUnlock(hDib);
    GlobalFree(hDib);
    CloseHandle(fh);
    return true;
}

bool SaveBWBmpFile(st_bw_image& input, LPWSTR lpFileName)
{
    BITMAPFILEHEADER bmfHdr; //位图文件头结构
    BITMAPINFOHEADER bi;     //位图信息头结构
    
    //设置位图信息头结构
    bi.biSize = sizeof (BITMAPINFOHEADER);  //本结构所占用字节数
    bi.biWidth = input.w;       //位图的宽度，以像素为单位
    bi.biHeight = input.h;      //位图的高度，以像素为单位
    bi.biPlanes = 1;            //目标设备的级别，必须为1
    bi.biBitCount = 1;          //每个像素所需的位数，1表示双色
    bi.biCompression = 0;       //位图压缩类型，0表示不压缩
    bi.biSizeImage = 0;         //位图的大小，以字节为单位
    bi.biXPelsPerMeter = 0;     //位图水平分辨率，每米像素数
    bi.biYPelsPerMeter = 0;     //位图垂直分辨率，每米像素数
    bi.biClrUsed = 0;           //位图实际使用的颜色表中的颜色数
    bi.biClrImportant = 0;      //位图显示过程中重要的颜色数
    
	//黑白位图的调色板
	RGBQUAD Palette[2] = { 0xFF, 0xFF, 0xFF, 0x0, 0,0,0,0 };

	//每一行数据的字节数必须是4字节的整数倍
	DWORD dwByteWidth = ((input.w + 31) / 32) * 4;
	DWORD dwInputByteWidth = ((input.w + 7) / 8);
    DWORD dwByteSize  = dwByteWidth * input.h;

	//重新排列位图数据
	BYTE* Dib =  new BYTE[dwByteSize];
	memset (Dib, 0xFF, dwByteSize); //默认值为白色

	int x8, y;
	for (y = 0; y < input.h; y++) {

		BYTE* line = Dib + dwByteWidth * y;
		BYTE* orig = input.img + dwInputByteWidth * (input.h - y - 1);
		for (x8 = 0; x8 < dwInputByteWidth; x8++) {

			line[x8] = orig[x8];
		}
	}
    
    // 创建位图文件
    ofstream of(lpFileName, ios::out | ios::binary | ios::trunc);
	if (of.bad()) {
        return false;
	}
    
    // 设置位图文件头
    bmfHdr.bfType = 0x4D42; //位图文件的类型，必须为"BM"
    bmfHdr.bfSize =         //位图文件的大小，以字节为单位
		  sizeof (BITMAPFILEHEADER)
        + sizeof (BITMAPINFOHEADER)
        + sizeof (Palette)
		+ dwByteSize;
    bmfHdr.bfReserved1 = 0; //位图文件保留字，必须为0
    bmfHdr.bfReserved2 = 0; //位图文件保留字，必须为0
    bmfHdr.bfOffBits =      //位图数据的起始位置，相对于位图文件头的偏移量
		  sizeof (BITMAPFILEHEADER)
        + sizeof (BITMAPINFOHEADER)
        + sizeof (Palette);
    
    // 写入文件
	of.write((LPSTR)&bmfHdr, sizeof(BITMAPFILEHEADER));
    of.write((LPSTR)&bi, sizeof(BITMAPINFOHEADER));
	of.write((LPSTR)&Palette, sizeof(Palette));
	of.write((LPSTR)Dib, dwByteSize);

	delete [] Dib;
	return true;
}

bool Save_4ColorImage_To_4BitBmpFile(st_four_color_image& input, LPWSTR lpFileName)
{
    BITMAPFILEHEADER bmfHdr; //位图文件头结构
    BITMAPINFOHEADER bi;     //位图信息头结构
    
    //设置位图信息头结构
	//（注：本函数采用4bit位图。）
	//（理论上2bit位图也可以，考虑到大多数读图软件的兼容能力，采用4bit位图）
    bi.biSize = sizeof (BITMAPINFOHEADER);  //本结构所占用字节数
    bi.biWidth = input.w;       //位图的宽度，以像素为单位
    bi.biHeight = input.h;      //位图的高度，以像素为单位
    bi.biPlanes = 1;            //目标设备的级别，必须为1
    bi.biBitCount = 4;          //每个像素所需的位数，4表示16色
    bi.biCompression = 0;       //位图压缩类型，0表示不压缩
    bi.biSizeImage = 0;         //位图的大小，以字节为单位
    bi.biXPelsPerMeter = 0;     //位图水平分辨率，每米像素数
    bi.biYPelsPerMeter = 0;     //位图垂直分辨率，每米像素数
    bi.biClrUsed = 0;           //位图实际使用的颜色表中的颜色数
    bi.biClrImportant = 0;      //位图显示过程中重要的颜色数
    
	//16色位图的调色板
	// RGBQ的实际排列顺序是BGR
	RGBQUAD Palette[16] = { 0,0,0,0,                // 黑
							0xFF, 0xFF, 0xFF, 0x0,  // 白
							0x0 , 0xFF, 0x0 , 0x0,  // 绿
							0x0 , 0x0 , 0xFF, 0x0,  // 红

							0,0,0,0,
							0,0,0,0,
							0,0,0,0,
							0,0,0,0,

							0,0,0,0,
							0,0,0,0,
							0,0,0,0,
							0,0,0,0,

							0,0,0,0,
							0,0,0,0,
							0,0,0,0,
							0,0,0,0 };

	//每一行数据的字节数必须是4字节的整数倍
	DWORD dwByteWidth = (((input.w / 2) + 3) / 4) * 4;
    DWORD dwByteSize  = dwByteWidth * input.h;

	//重新排列位图数据
	BYTE* Dib =  new BYTE[dwByteSize];
	memset (Dib, 0xFF, dwByteSize);//全填1，写入时用与操作

	int x, y;
	for (y = 0; y < input.h; y++) {

		BYTE* line = Dib + dwByteWidth * y;
		char* orig = input.img + input.w * (input.h - y - 1);

		for (x = 0; x < input.w; x++) {

			BYTE* abyte = line + (x / 2);
			BYTE mask = 0xFF;
			if (0 == x % 2) {
				// 写高半字节
				switch (orig[x]) {
				case 1:  mask = 0x1F; break;
				case 2:  mask = 0x2F; break;
				case 3:  mask = 0x3F; break;
				case 0:
				default: mask = 0x0F; break;
				}
			}
			else {
				// 写低半字节
				switch (orig[x]) {
				case 1:  mask = 0xF1; break;
				case 2:  mask = 0xF2; break;
				case 3:  mask = 0xF3; break;
				case 0:
				default: mask = 0xF0; break;
				}
			}

			*abyte &= mask;
		}
	}
    
    // 创建位图文件
    ofstream of(lpFileName, ios::out | ios::binary | ios::trunc);
	if (of.bad()) {
        return false;
	}
    
    // 设置位图文件头
    bmfHdr.bfType = 0x4D42; //位图文件的类型，必须为"BM"
    bmfHdr.bfSize =         //位图文件的大小，以字节为单位
		  sizeof (BITMAPFILEHEADER)
        + sizeof (BITMAPINFOHEADER)
        + sizeof (Palette)
		+ dwByteSize;
    bmfHdr.bfReserved1 = 0; //位图文件保留字，必须为0
    bmfHdr.bfReserved2 = 0; //位图文件保留字，必须为0
    bmfHdr.bfOffBits =      //位图数据的起始位置，相对于位图文件头的偏移量
		  sizeof (BITMAPFILEHEADER)
        + sizeof (BITMAPINFOHEADER)
        + sizeof (Palette);
    
    // 写入文件
	of.write((LPSTR)&bmfHdr, sizeof(BITMAPFILEHEADER));
    of.write((LPSTR)&bi, sizeof(BITMAPINFOHEADER));
	of.write((LPSTR)&Palette, sizeof(Palette));
	of.write((LPSTR)Dib, dwByteSize);

	of.close();

	delete [] Dib;
	return true;
}

bool Save_4ColorImage_To_2BitBmpFile(st_four_color_image& input, LPWSTR lpFileName)
{
    BITMAPFILEHEADER bmfHdr; //位图文件头结构
    BITMAPINFOHEADER bi;     //位图信息头结构
    
    //设置位图信息头结构
	//（注：本函数采用2bit位图。）
	//（如果位图编辑软件不支持此格式，可换用4bit位图）
    bi.biSize = sizeof (BITMAPINFOHEADER);  //本结构所占用字节数
    bi.biWidth = input.w;       //位图的宽度，以像素为单位
    bi.biHeight = input.h;      //位图的高度，以像素为单位
    bi.biPlanes = 1;            //目标设备的级别，必须为1
    bi.biBitCount = 2;          //每个像素所需的位数，2表示4色
    bi.biCompression = 0;       //位图压缩类型，0表示不压缩
    bi.biSizeImage = 0;         //位图的大小，以字节为单位
    bi.biXPelsPerMeter = 0;     //位图水平分辨率，每米像素数
    bi.biYPelsPerMeter = 0;     //位图垂直分辨率，每米像素数
    bi.biClrUsed = 0;           //位图实际使用的颜色表中的颜色数
    bi.biClrImportant = 0;      //位图显示过程中重要的颜色数
    
	//16色位图的调色板
	// RGBQ的实际排列顺序是BGR
	RGBQUAD Palette[4]={0,0,0,0,                // 黑
						0xFF, 0xFF, 0xFF, 0x0,  // 白
						0x0 , 0xFF, 0x0 , 0x0,  // 绿
						0x0 , 0x0 , 0xFF, 0x0}; // 红

	//每一行数据的字节数必须是4字节的整数倍
	DWORD dwByteWidth = (((input.w / 4) + 3) / 4) * 4;
    DWORD dwByteSize  = dwByteWidth * input.h;

	//重新排列位图数据
	BYTE* Dib =  new BYTE[dwByteSize];
	memset (Dib, 0xFF, dwByteSize);//全填1，写入时用与操作

	int x, y;
	for (y = 0; y < input.h; y++) {

		BYTE* line = Dib + dwByteWidth * y;
		char* orig = input.img + input.w * (input.h - y - 1);

		for (x = 0; x < input.w; x++) {

			BYTE* abyte = line + (x / 4);
			BYTE mask = 0xFF;
			switch (x % 4) {
			case 0:
				// 写 XX -- -- --
				switch (orig[x]) {
				case 1:  mask = 0x7F; break;
				case 2:  mask = 0xBF; break;
				case 3:  mask = 0xFF; break;
				case 0:
				default: mask = 0x3F; break;
				}
				break;
			case 1:
				// 写 -- XX -- --
				switch (orig[x]) {
				case 1:  mask = 0xDF; break;
				case 2:  mask = 0xEF; break;
				case 3:  mask = 0xFF; break;
				case 0:
				default: mask = 0xCF; break;
				}
				break;
			case 2:
				// 写 -- -- XX --
				switch (orig[x]) {
				case 1:  mask = 0xF7; break;
				case 2:  mask = 0xFB; break;
				case 3:  mask = 0xFF; break;
				case 0:
				default: mask = 0xF3; break;
				}
				break;
			case 3:
				// 写 -- -- -- XX
				switch (orig[x]) {
				case 1:  mask = 0xFD; break;
				case 2:  mask = 0xFE; break;
				case 3:  mask = 0xFF; break;
				case 0:
				default: mask = 0xFC; break;
				}
				break;
			}

			*abyte &= mask;
		}
	}
    
    // 创建位图文件
    ofstream of(lpFileName, ios::out | ios::binary | ios::trunc);
	if (of.bad()) {
        return false;
	}
    
    // 设置位图文件头
    bmfHdr.bfType = 0x4D42; //位图文件的类型，必须为"BM"
    bmfHdr.bfSize =         //位图文件的大小，以字节为单位
		  sizeof (BITMAPFILEHEADER)
        + sizeof (BITMAPINFOHEADER)
        + sizeof (Palette)
		+ dwByteSize;
    bmfHdr.bfReserved1 = 0; //位图文件保留字，必须为0
    bmfHdr.bfReserved2 = 0; //位图文件保留字，必须为0
    bmfHdr.bfOffBits =      //位图数据的起始位置，相对于位图文件头的偏移量
		  sizeof (BITMAPFILEHEADER)
        + sizeof (BITMAPINFOHEADER)
        + sizeof (Palette);
    
    // 写入文件
	of.write((LPSTR)&bmfHdr, sizeof(BITMAPFILEHEADER));
    of.write((LPSTR)&bi, sizeof(BITMAPINFOHEADER));
	of.write((LPSTR)&Palette, sizeof(Palette));
	of.write((LPSTR)Dib, dwByteSize);

	of.close();

	delete [] Dib;
	return true;
}